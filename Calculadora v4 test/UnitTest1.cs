using CalculadoraOperaciones;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Calculadora_v4_test
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void testSumar()
        {
            double operando1 = 3.7;
            double operando2 = 0.00000000000000000001;
            Assert.AreEqual(operando1 + operando2, Operaciones.Sumar(operando1, operando2));
        }

        [TestMethod]
        public void testRestar()
        {
            double operando1 = 3.7;
            double operando2 =2.1;
            Assert.AreEqual(operando1 - operando2, Operaciones.Restar(operando1, operando2));
        }

        [TestMethod]
        public void testMultiplicar()
        {
            double operando1 = 3.7;
            double operando2 = 0.00000000000000000001;
            Assert.AreEqual(operando1 * operando2, Operaciones.Multiplicar(operando1, operando2));
        }

        [TestMethod]
        public void testDividir()
        {
            double operando1 = 3.7;
            double operando2 = 0;
            Assert.AreEqual(operando1 / operando2, Operaciones.Dividir(operando1, operando2));
        }

        //[TestMethod]
        //public void testDividirPorCero()
        //{
        //    double operando1 = 3.7;
        //    double operando2 = 0;
        //    Assert.ThrowsException<System.DivideByZeroException>(() => Operaciones.Dividir(operando1, operando2));
        //}



    }
}
